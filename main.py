import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from cargararchivos import DlgFileChooser
from guardararchivos import DlgFileSaver
import dialogo
import image
import datos


class MainWindow():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")
        self.window = self.builder.get_object("window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Visor PDB")
        self.window.resize(800, 600)
        self.ruta = ""
        self.folder = ""
        self.btnabrir = self.builder.get_object("btnabrir")
        self.btnabrir.connect("clicked", self.btn_filechooser, "open")
        self.btnguardar = self.builder.get_object("btnguardar")
        self.btnguardar.connect("clicked", self.btn_filesaver, "save")
        self.dataframe = 0
        self.btninfo = self.builder.get_object("btninfo")
        self.btninfo.connect("clicked", self.abrir_dlgwindow)
        self.lblnombre = self.builder.get_object("lblnombre")
        self.lblinfo = self.builder.get_object("lblinfo")
        self.statuslbl = self.builder.get_object("statuslbl")
        self.combobox = self.builder.get_object("combobox")
        self.combobox.set_entry_text_column(0)
        lista = ["ATOM", "HETATM", "ANISOU", "OTHERS"]
        for index in lista:
            self.combobox.append_text(index)
        self.combobox.connect("changed", self.cambio_combobox)
        self.pandastree = self.builder.get_object("treeview")
        self.image = self.builder.get_object("image")
        self.window.show_all()

    def btn_filechooser(self, btn=None, opt=None):
        filechooser = DlgFileChooser(option=opt)
        dialogo = filechooser.dialogo
        if opt == "open":
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name("PDB_Files")
            filter_pdb.add_pattern("*.pdb")
            dialogo.add_filter(filter_pdb)
        response = dialogo.run()
        if response == Gtk.ResponseType.OK:
            self.ruta = dialogo.get_filename()
            image.obtener_imagen(self.ruta)
            self.image.set_from_file("imagen.png")
            temp = []
            temp = self.ruta.split("/")
            self.lblnombre.set_text(f"Archivo cargado: {temp[-1]}")
            dataframe = datos.cargardatos(self.ruta)
            dfothers = dataframe["OTHERS"]
            titulo = dfothers.iloc[1][1]
            clase = dfothers.iloc[0][1]
            self.lblinfo.set_text(f"Título:{titulo}\n"
                                  f"Clasificación:{clase}")
            dialogo.destroy()
        else:
            dialogo.destroy()

    def abrir_dlgwindow(self, btn=None):
        dialogo.GtkWindow()

    def cambio_combobox(self, cmb=None):
        if self.ruta == "":
            self.statuslbl.set_text("Ningún archivo seleccionado")
        else:
            if self.pandastree.get_columns():
                for column in self.pandastree.get_columns():
                    self.pandastree.remove_column(column)
            it = self.combobox.get_active()
            print(self.ruta)
            self.dataframe = datos.cargardatos(self.ruta)
            if it == 0:
                self.dataframe = self.dataframe["ATOM"]
            elif it == 1:
                self.dataframe = self.dataframe["HETATM"]
            elif it == 2:
                self.dataframe = self.dataframe["ANISOU"]
            elif it == 3:
                self.dataframe = self.dataframe["OTHERS"]
            len_columns = len(self.dataframe.columns)
            self.model = Gtk.ListStore(*(len_columns * [str]))
            self.pandastree.set_model(model=self.model)
            cell = Gtk.CellRendererText()
            for i in range(len_columns):
                column = Gtk.TreeViewColumn(self.dataframe.columns[i],
                                            cell,
                                            text=i)
                self.pandastree.append_column(column)
            for value in self.dataframe.values:
                row = [str(i) for i in value]
                self.model.append(row)
            temp = []
            temp = self.ruta.split("/")
            self.lblnombre.set_text(f"Archivo cargado: {temp[-1]}")

    def btn_filesaver(self, btn=None, opt=None):
        if type(self.dataframe) == int:
            self.statuslbl.set_text("Debe seleccionar una opción para guardar")
        else:
            filesaver = DlgFileSaver(option=opt)
            dialogo = filesaver.dialogo
            response = dialogo.run()
            if response == Gtk.ResponseType.OK:
                self.folder = dialogo.get_current_folder()
                text = filesaver.entry.get_text()
                if text == "":
                    self.statuslbl.set_text("Error al guardar: El archivo debe"
                                            " tener un nombre")
                else:
                    archivo = open(f"{self.folder}/{text}", "w")
                    temp = self.dataframe.to_string()
                    archivo.write(temp)
                    archivo.close()
                    self.statuslbl.set_text("Archivo guardado correctamente")
                dialogo.destroy()
            else:
                dialogo.destroy()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()
