==== Proyecto 2 >> Visor PDB ====

Este proyecto está orientado al procesamiento de cualquier archivo de tipo PDB, con funciones orientadas a obtener información relevante del archivo en cuestión, mostrando una imagen de la proteína seleccionada, además de poder mostrar coordenadas atómicas de los residuos estándar (ATOM), coordenadas atómicas de los átomos que pertenecen a los grupos no estándar (HETATM), los factores de temperatura anisotrópicos (ANISOU), y otra información relevante (OTHERS). El programa tiene la capacidad de elegir el archivo que se desea cargar, además de dar la opción de guardar como archivo de texto la información mostrada.

==== Pre-Requisitos y Ejecución del Programa ====

Tener instalado Python 3


Tener instalado el repositorio Pandas

Tener instalado PyMol

Tener instalado GTK+3.0

Para ejecutar este proyecto se debe abrir el archivo main.py con Python 3. Una vez abierto, se abrirá una ventana la cual contiene 3 botones y un label en la parte superior; el botón Abrir abre una ventana de tipo filechooser que da la posibilidad de cargar un archivo PDB. El botón Guardar solo da la posibilidad de guardar un archivo cuando el usuario ha cargado algún archivo PDB, y ha seleccionado alguna opción el combobox que se muestra mas abajo. El label muestra información respecto al estado de algunos botones, por ejemplo, si el usuario presiona el botón Guardar sin haber seleccionado una opción en el combobox, el label le avisará al usuario que debe seleccionar una opción antes de guardar. Por último, el botón Acerca De muestra información de la aplicación, como el nombre de ésta, su versión, el nombre de los desarrolladores, entre otros.

En la zona media de la venta principal, se encuentran dos labels y un combobox. Los labels muestran información del archivo cargado, como por ejemplo el nombre del archivo, el título de la proteína cargada, y el tipo de proteína. El combobox muestra 4 opciones: ATOM, HETATM, ANISOU y OTHERS, que son el tipo de información que el programa puede mostrar en el GtkTreeView que hay mas abajo.

Por último, en la parte inferior hay dos áreas dedicadas a la visualización de la información obtenida al cargar el archivo. En la parte izquierda se muestra un GtkTreeView que muestra la información seleccionada en el combobox. En la parte derecha se muestra un GtkImage que muestra una imagen de la proteína cargada.

==== Funcionamiento ====

Este proyecto consta de 6 archivos .py, una carpeta que contiene un archivo .ui con la interfaz gráfica creada en Glade, y un archivo PDB de prueba. El archivo main.py es el principal, pues contiene los objetos de la ventana principal y une a todos los demás archivos .py. Los archivos image.py y datos.py procesan el archivo PDB para obtener una imagen de la proteína, y un DataFrame de la información que hay en el archivo. Los archivos cargararchivos.py y guardararchivos.py son ventanas de tipo FileChooser. Una está orientada a elegir un archivo de tipo PDB para cargar, y la otra se encarga de elegir el nombre y la ruta en donde se quiere guardar el archivo de texto. El archivo dialogo.py es una ventana de tipo GtkAboutDialog con la información del progama.

Luego de ejecutar el programa, se creará una imagen de la proteína cargada con el nombre imagen.png, y si el usario ha seleccionado la opción de guardar, se creará un archivo de texto con la información que el usuario seleccionó en el combobox, en la ruta que el usuario haya escogido, y con un nombre puesto por el usuario.

==== Bugs y Errores ====

En el filechooser para guardar un archivo, si el usuario ha seleccionado una directorio, el botón Guardar en vez de guardar el archivo, abrirá el directorio. Para solucionar esto, el usuario debe seleccionar cualquier archivo que no sea un directorio, y presionar Guardar.

Si el archivo PDB es demasiado pesado, el programa se demorará más de lo normal en mostrar la información seleccionada en el combobox.

==== CONSTRUIDO CON ====

Vim - the ubiquitous text editor

Glade - A User Interface Designer

==== Desarrollo ====

Este programa fue desarrollado por Daniel Marcelo Tobar Hormazábal, José Ignacio Valenzuela Soto y Yostin Nirvan Sepúlveda Caniqueo
