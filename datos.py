from biopandas.pdb import PandasPdb


def cargardatos(ruta):
    ppdb = PandasPdb()
    ppdb.read_pdb(ruta)
    return(ppdb.df)


if __name__ == "__main__":
    cargardatos()
