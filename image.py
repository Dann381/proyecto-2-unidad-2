from biopandas.pdb import PandasPdb
import pymol
import __main__


def obtener_imagen(ruta):
    ppdb = PandasPdb()
    ppdb.read_pdb(ruta)
    __main__.pymol_argv = ["pymol", "-qc"]
    pymol.finish_launching()
    png_file = "imagen"
    lista = []
    lista = ruta.split("/")
    pdb_name = lista[-1]
    pdb_file = ruta
    pymol.cmd.load(pdb_file, pdb_name)
    pymol.cmd.disable("all")
    pymol.cmd.enable(pdb_name)
    pymol.cmd.hide("all")
    pymol.cmd.show("cartoon")
    pymol.cmd.set("ray_opaque_background", 0)
    pymol.cmd.pretty(pdb_name)
    pymol.cmd.png(png_file)
    pymol.cmd.ray()


if __name__ == "__main__":
    obtener_imagen()
