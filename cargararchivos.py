import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DlgFileChooser():

    def __init__(self, option=None):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")
        self.dialogo = self.builder.get_object("filechooser")
        self.dialogo.resize(800, 600)
        if option == "open":
            self.dialogo.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN,
                                     Gtk.ResponseType.OK)
        self.dialogo.show_all()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()
