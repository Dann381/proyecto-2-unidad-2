import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DlgFileSaver():

    def __init__(self, option=None):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")
        self.dialogo = self.builder.get_object("filesaver")
        self.dialogo.resize(800, 600)
        self.entry = self.builder.get_object("entry")
        self.entry.set_text("archivo.txt")
        if option == "save":
            self.dialogo.add_buttons(Gtk.STOCK_SAVE,
                                     Gtk.ResponseType.OK)
        self.dialogo.show_all()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()
