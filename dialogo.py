import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class GtkWindow():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")
        self.dlgwindow = self.builder.get_object("gtkdialog")
        self.dlgwindow.resize(400, 300)
        self.dlgwindow.set_title("Información")
        self.dlgwindow.show_all()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()
